module.exports = {
    configureWebpack: config => {
		config.devtool = 'source-map'
	},

    // pwa: {
    //     name: 'إحصائيات فيروس كورونا في المغرب',
    //     themeColor: '#FFFFFF',
    //     appleMobileWebAppCapable: 'yes',
    //     appleMobileWebAppStatusBarStyle: 'black-translucent'
    // },

    pluginOptions: {
      i18n: {
        locale: 'ar',
        fallbackLocale: 'ar',
        localeDir: 'locales',
        enableInSFC: false
      }
    }
};
