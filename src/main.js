import Vue from 'vue'
import App from './App.vue'
// import './registerServiceWorker'
import HighchartsVue from 'highcharts-vue'
import router from './router'
import store from './store'

import '@/assets/tailwind.css'

import VuePageTransition from 'vue-page-transition'
import VueAnalytics from 'vue-analytics';
import Vue2Filters from 'vue2-filters'
import i18n from './i18n'

Vue.config.productionTip = false
Vue.use(Vue2Filters)
Vue.use(VuePageTransition)
Vue.use(HighchartsVue)
Vue.use(VueAnalytics, {
  id: 'UA-116685821-3',
  router
});


new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
