import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    dark: false
  },
  mutations: {
    changeDark (state, payload) {
      state.dark = payload
    }
  },
  // actions: {
  //   activateDark({commit, res}) {
  //     commit('changeDark', res)
  //   },
  //   desactivateDark({commit, res}) {
  //     commit('changeDark', res)
  //   }
  // },
  modules: {
  }
})
