import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import i18n from '../i18n'

Vue.use(VueRouter)

let router = new VueRouter({
  routes: [
    {
      path: '/',
      redirect: `/${i18n.locale}`
    },
    {
      path: '/:lang',
      component: {
        render(h) {
          return h('router-view')
        },
      },
      children: [
        {
          path: '/',
          name: 'Home',
          component: Home
        },
        {
          path: 'stats',
          name: 'Stats',
          component: () => import(/* webpackChunkName: "about" */ '../views/Stats.vue')
        },
        {
          path: 'news',
          name: 'News',
          component: () => import(/* webpackChunkName: "about" */ '../views/News.vue')
        },
        {
          path: 'infos',
          name: 'Infos',
          component: () => import(/* webpackChunkName: "about" */ '../views/Infos.vue')
        },
        {
          path: 'links',
          name: 'Links',
          component: () => import(/* webpackChunkName: "about" */ '../views/Links.vue')
        },
        {
          path: 'about',
          name: 'About',
          component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
        }
      ]
    },
    { 
      path: '*',
      component: () => import(/* webpackChunkName: "about" */ '../views/Home.vue')
    }
  ]
})
router.beforeEach((to, from, next) => {
  let language = to.params.lang
  if(!language)
    language = "ar"
  i18n.locale = language
  next()
})

export default router
